#!/usr/local/bin/python

import sys
from dagster_graphql import DagsterGraphQLClient
from dagster_graphql import ReloadRepositoryLocationInfo
from dagster_graphql import ReloadRepositoryLocationStatus

REPO_NAME = "dagster_project"
PORT = int("{{ dagster_localhost_port }}")

client = DagsterGraphQLClient("localhost", port_number=PORT)
reloadinfo = client.reload_repository_location(REPO_NAME)

if reloadinfo.status != ReloadRepositoryLocationStatus.SUCCESS:
    print(reloadinfo.message)
    sys.exit(1)
    pass
