version: "3.7"

services:
  # This service runs the postgres DB used by dagster for run storage, schedule storage,
  # and event log storage.
  docker_example_postgresql:
    image: postgres:11
    container_name: docker_example_postgresql
    environment:
      POSTGRES_USER: "dagster"
      POSTGRES_PASSWORD: "dagster"
      POSTGRES_DB: "dagster"
    networks:
      - docker_example_network

  # This service runs the gRPC server that loads your user code, in both dagit
  # and dagster-daemon. By setting DAGSTER_CURRENT_IMAGE to its own image, we tell the
  # run launcher to use this same image when launching runs in a new container as well.
  # Multiple containers like this can be deployed separately - each just needs to run on
  # its own port, and have its own entry in the workspace.yaml file that's loaded by dagit.
  docker_example_user_code:
    build:
      context: .
      dockerfile: ./dockerfile-usercode
    container_name: docker_example_user_code
    image: docker_example_user_code_image
    restart: always
    environment:
      DAGSTER_POSTGRES_USER: "dagster"
      DAGSTER_POSTGRES_PASSWORD: "dagster"
      DAGSTER_POSTGRES_DB: "dagster"
      DAGSTER_CURRENT_IMAGE: "docker_example_user_code_image"
    volumes:
      - {{ dagster_setup_dir }}/app:{{ dagster_setup_dir }}/app
    networks:
      - docker_example_network

  # This service runs dagit, which loads your user code from the user code container.
  # Since our instance uses the QueuedRunCoordinator, any runs submitted from dagit will be put on
  # a queue and later dequeued and launched by dagster-daemon.
  docker_example_dagit:
    build:
      context: .
      dockerfile: ./dockerfile-dagster
    entrypoint:
      - dagit
      - -h
      - "0.0.0.0"
      - -p
      - "{{ dagster_localhost_port }}"
      - -w
      - workspace.yaml
    container_name: docker_example_dagit
    expose:
      - "{{ dagster_localhost_port }}"
    ports:
      - "127.0.0.1:{{ dagster_localhost_port }}:{{ dagster_localhost_port }}"
    environment:
      DAGSTER_POSTGRES_USER: "dagster"
      DAGSTER_POSTGRES_PASSWORD: "dagster"
      DAGSTER_POSTGRES_DB: "dagster"
    volumes: # Make docker client accessible so we can terminate containers from dagit
      - /var/run/docker.sock:/var/run/docker.sock
      - /tmp/io_manager_storage:/tmp/io_manager_storage
      - {{ dagster_setup_dir }}/app:{{ dagster_setup_dir }}/app
    networks:
      - docker_example_network
    depends_on:
      - docker_example_postgresql
      - docker_example_user_code

  # This service runs the dagster-daemon process, which is responsible for taking runs
  # off of the queue and launching them, as well as creating runs from schedules or sensors.
  docker_example_daemon:
    build:
      context: .
      dockerfile: ./dockerfile-dagster
    entrypoint:
      - dagster-daemon
      - run
    container_name: docker_example_daemon
    restart: on-failure
    environment:
      DAGSTER_POSTGRES_USER: "dagster"
      DAGSTER_POSTGRES_PASSWORD: "dagster"
      DAGSTER_POSTGRES_DB: "dagster"
    volumes: # Make docker client accessible so we can launch containers using host docker
      - /var/run/docker.sock:/var/run/docker.sock
      - /tmp/io_manager_storage:/tmp/io_manager_storage
    networks:
      - docker_example_network
    depends_on:
      - docker_example_postgresql
      - docker_example_user_code

networks:
  docker_example_network:
    driver: bridge
    name: docker_example_network
