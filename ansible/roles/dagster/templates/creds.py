portalCreds = {
    "certificate" : {{ geni_rpccert | to_json }},
    "sshpubkey"   : {{ geni_pubkey | to_json }},
    "debug"       : False
}

managerCreds = {
    "private_key"   : {{ geni_key | to_json }},
    "conn_timeout"  : 10,
    "look_for_keys" : False,
    "allow_host_key_change": True
}

kiwiCreds = {
    "url"      : "https://{{ emulab_fqdn }}:8544/xml-rpc/",
    "username" : "admin",
    "password" : "{{ dagster_admin_password }}"
}
