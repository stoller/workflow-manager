CSRF_TRUSTED_ORIGINS = [ "http://{{ emulab_fqdn }}", "http://{{ emulab_fqdn }}:8544",  "https://{{ emulab_fqdn }}", "https://{{ emulab_fqdn }}:8544", "http://{{ emulab_fqdn | lower }}", "http://{{ emulab_fqdn | lower }}:8544",  "https://{{ emulab_fqdn | lower }}", "https://{{ emulab_fqdn | lower }}:8544" ]
USE_TZ = True
KIWI_TIME_ZONE = "America/Denver"
FILE_UPLOAD_MAX_SIZE = 1073741824
DATA_UPLOAD_MAX_MEMORY_SIZE = int(FILE_UPLOAD_MAX_SIZE * 1.5)
FILE_UPLOAD_MAX_MEMORY_SIZE = 1073741824
